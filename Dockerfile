FROM nginx:stable-alpine
LABEL maintainer="Ing. Ján Švantner <janci@janci.net>"

COPY nginx-host.conf  /etc/nginx/conf.d/default.conf

